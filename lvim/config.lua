---
--- Keybinding
---

-- Toggle NvimTree with F3
vim.api.nvim_set_keymap('n', '<F3>', ':NvimTreeToggle<CR>', { noremap = true, silent = true })
vim.api.nvim_set_keymap('i', '<F3>', '<Esc>:NvimTreeToggle<CR>a', { noremap = true, silent = true })
